<?php

namespace La\LazyLoadBundle\Tests\Twig\Extension;

use La\LazyLoadBundle\Twig\Extension\LazyExtension;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class LazyExtensionTest
 * @package La\LazyLoadBundle\Tests\Twig\Extension
 */
class LazyExtensionTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Test testLazyHtml method.
     */
    public function testLazyHtml()
    {
        $lazyExtension = $this->getService();

        $img1 = '<img src="toto.jpg" class="classtest" />';

        $crawler = new Crawler($lazyExtension->lazyHtml($img1));
        $node = $crawler->filter('img[data-src="toto.jpg"]');
        $this->assertEquals(1, $node->count());
        $node = $crawler->filter('img[src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"]');
        $this->assertEquals(1, $node->count());
        $node = $crawler->filter('img[class="classtest la_lazy_image"]');
        $this->assertEquals(1, $node->count());

        $crawler = new Crawler($lazyExtension->lazyHtml($img1, false));
        $node = $crawler->filter('img[data-src="toto.jpg"]');
        $this->assertEquals(0, $node->count());
        $node = $crawler->filter('img[src="toto.jpg"]');
        $this->assertEquals(1, $node->count());
        $node = $crawler->filter('img[class="classtest"]');
        $this->assertEquals(1, $node->count());

    }

    /**
     * Test getName method.
     */
    public function testGetName()
    {
        $this->assertEquals('lazy_image_extension', $this->getService()->getName());
    }

    /**
     * Return an instance of LazyExtension.
     *
     * @return LazyExtension
     */
    private function getService()
    {
        return new LazyExtension();
    }

    /**
     * test getFilters method
     */
    public function testGetFilters()
    {
        $extension = $this->getExtension();
        $filters = $extension->getFilters();

        $this->assertInstanceOf('Twig_SimpleFilter', $filters['lazyHtml']);
    }


    /**
     * @return LazyExtension
     */
    public function getExtension()
    {
        $extension = new LazyExtension();

        return $extension;
    }


}
