Changelog
=========


### 2.1.0 (2015-11-19)

* Séparation du script unveil et de l'appel js.

### 2.0.2 (2015-07-06)

* readme

### 2.0.1 (2015-07-06)

* parametrage de limage data

### 2.0.0 (2015-06-24)

* migration SF 2.7

### 1.0.1 (2015-06-24)

* cleanup

### 1.0.0 (2015-06-21)

* test

### 0.1 (2015-06-10)

* init
