LaLazyLoadBundle
----------------

Ce bundle ajoute, au niveau du templating Twig, le node **lazy** permettant de transformer automatiquement les balises img en
système de lazyloading.  

Fonctionnement
==============

La src de l'image est remplacée par une image transparente.
Les images retrouvent leur src originales quand elle deviennent visibles, parr ex au scrool de la page.

Pour ce faire il suffit d'encapsuler la balise img avec le node {% lazy %}

ex:

    {% lazy %}
    <img src="http://toto.com/titi.jpg" alt="" />
    {% endlazy %}

Le node accepte en paramètre un boolean qui active/desactive le lazyloading de l'image.

    {% lazy true %}
    <img src="http://toto.com/titi.jpg" alt="bla" class="img-responsive" />
    {% endlazy %}

    {% lazy false %}
    <img src="http://toto.com/toto.jpg" alt="bla" class="img-responsive" />
    {% endlazy %}

Pour fonctionner, le système a besoin du js **Resources/public/js/unveil/jquery.unveil.js** ainsi que de **jQuery**

Configuration
=============

le src de l'image de remplacement peut être spécifié via le paramètre **data_image**

ex:

    la_lazy_load:
        data_image: data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7


Transformation du code
======================

Le lazyloading transforme :   

- l'attribut src en data-src   
- change l'attribut src en y injectant une image transparente en data (data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)
- ajoute la classe **la_lazy_image**   


Les attributs natifs de l'image sont réinjectés dans le code transformé (class, id, etc.)

ex :

&lt;img data-src="http://toto.com/titi.jpg" alt="bla" class="img-responsive la_lazy_image" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" /&gt;
