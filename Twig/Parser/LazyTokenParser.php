<?php


namespace La\LazyLoadBundle\Twig\Parser;

use La\LazyLoadBundle\Twig\Node\LazyNode;
use Twig_Token;

/**
 * LazyTokenParser
 *
 * @author N'Diaye Abdoul <wn-a.ndiaye@lagardere-active.com>
 *
 */
class LazyTokenParser extends \Twig_TokenParser
{

    /**
     * Parse Lazy Tag
     *
     * @param Twig_Token $token
     * @return LazyNode
     * @throws \Twig_Error_Syntax
     */
    public function parse(Twig_Token $token)
    {

        $lineno = $token->getLine();
        $stream = $this->parser->getStream();
        $values = null;
        if ($stream->test(Twig_Token::NAME_TYPE)) {
            $values = $this->parser->getExpressionParser()->parseMultitargetExpression();
        }
        $stream->expect(Twig_Token::BLOCK_END_TYPE);
        $body = $this->parser->subparse(array($this, 'decideBlockEnd'), true);
        $stream->expect(Twig_Token::BLOCK_END_TYPE);

        return new LazyNode($body, $lineno, $values);
    }

    /**
     * decideForEnd
     * @param \Twig_Token $token
     *
     * @return bool
     *
     */
    public function decideBlockEnd(\Twig_Token $token)
    {
        return $token->test('endlazy');
    }

    /**
     * Get Tag Name
     * @return string
     *
     */
    public function getTag()
    {
        return 'lazy';
    }
}
