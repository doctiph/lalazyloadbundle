<?php


namespace La\LazyLoadBundle\Twig\Node;

/**
 * Class LazyNode
 * @package La\LazyLoadBundle\Twig\Node
 */
class LazyNode extends \Twig_Node
{
    /**
     * Values
     * @var \Twig_Node
     */
    protected $values;

    /**
     * Construct
     * @param \Twig_Node $body
     * @param array $lineno
     * @param array $values
     */
    public function __construct(
        \Twig_Node $body,
        $lineno,
        $values
    ) {
        parent::__construct(array('body' => $body, 'values' => $values), [], $lineno);
    }

    /**
     * Transform node attribute to lazy
     * @param \Twig_Compiler $compiler
     */
    public function compile(\Twig_Compiler $compiler)
    {
        $compiler
            ->addDebugInfo($this)
            ->write("ob_start();\n")
            ->subcompile($this->getNode('body'))
            ->write('echo $this->env->getExtension("La\LazyLoadBundle\Twig\Extension\LazyExtension")->lazyHtml(ob_get_clean(),');
        if ($this->getNode('values') !== null) {
            $compiler->subcompile($this->getNode('values'));
        } else {
            $compiler->write('true');
        }
        $compiler->write(');');
    }
}
