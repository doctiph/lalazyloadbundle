<?php

namespace La\LazyLoadBundle\Twig\Extension;

use La\LazyLoadBundle\Twig\Parser\LazyTokenParser;

/**
 * Class LazyExtension
 * @package La\LazyLoadBundle\Twig\Extension
 */
class LazyExtension extends \Twig_Extension
{

    protected $classname = 'la_lazy_image';

    protected $transparentImage;

    public function __construct($dataImage)
    {
        $this->transparentImage = $dataImage;
    }

    /**Twig
     * Declaration of LazyTokenParser
     * Add tag "lazy"
     * @return LazyTokenParser[]
     *
     */
    public function getTokenParsers()
    {
        return array(new LazyTokenParser('lazy'));
    }

    /**
     * Get Filter
     * @return array
     */
    public function getFilters()
    {
        return array(
             new \Twig_SimpleFilter('lazyHtml', array($this, 'lazyHtml')),
        );
    }


    /**
     * Lazy Load Image
     *
     * @param $htmlNode
     * @param bool $mustLazy
     * @return mixed
     */
    public function lazyHtml($htmlNode, $mustLazy = true)
    {
        if ($mustLazy === false) {
            return $htmlNode;
        }
        $data = preg_replace_callback('/<img[^>]+>/sx', array(&$this, 'transformNode'), $htmlNode, -1);

        return $data;
    }

    /**
     * Transform "a" node to "span" node
     * @param string $node
     *
     * @return string
     */
    protected function transformNode($node)
    {
        $result = '<img';
        $pattern = '/(\\w+)\s*=\\s*("[^"]*"|\'[^\']*\'|[^"\'\\s>]*)/';
        preg_match_all($pattern, $node[0], $matches, PREG_SET_ORDER);
        $hasClassAttribute = false;

        foreach ($matches as &$match) {
            $match[2] = str_replace(array('"', '\''), '', $match[2]);

            switch ($match[1]) {
                case 'class':
                    $match[2] .= ' ' . $this->classname;
                    $hasClassAttribute = true;
                    break;
                case 'src' :
                    $match[1] = 'data-src';
                    break;
            }
            $result .= ' ' . $match[1] . '="' . $match[2] . '"';
        }
        if (!$hasClassAttribute) {
            $result .= ' class="' . $this->classname . '"';
        }
        $result .= ' src="' . $this->transparentImage . '"';

        $result .= ' />';

        return $result;
    }
}
